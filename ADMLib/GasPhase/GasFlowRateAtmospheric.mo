within ADMLib.GasPhase;

model GasFlowRateAtmospheric
  extends ADMLib.Interfaces.TotalGasPressure;
  ADMLib.Types.PipeCoefficient kp;
  ADMLib.Types.VolumetricFlow Qgas;
  ADMLib.Types.Pressure PartialPressures[1, :];
  Modelica.SIunits.Pressure Patm = 101325;
equation
  Qgas = kp * (TotalPgas - Patm) * (TotalPgas / Patm);
  PartialPressures1 = PartialPressures;
end GasFlowRateAtmospheric;