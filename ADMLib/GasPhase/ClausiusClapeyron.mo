within ADMLib.GasPhase;

model ClausiusClapeyron
  ADMLib.Types.Pressure a;
  ADMLib.Types.Temperature b;
  ADMLib.Types.Temperature Tbase = 298.15;
  ADMLib.Types.Temperature Top;
  ADMLib.Types.Pressure Pgas;
equation
  Pgas = a * exp(b * (1 / Tbase - 1 / Top));
end ClausiusClapeyron;