within ADMLib.HeatTransfer;

model SimpleTemperature
  Modelica.SIunits.Temp_C T;
  ADMLib.Types.VelocityConstant k;
equation
  k = 0.013 * T - 0.129;
end SimpleTemperature;