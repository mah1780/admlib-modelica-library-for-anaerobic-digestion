within ADMLib.HeatTransfer;

model VesselTemperature
  extends ADMLib.Interfaces.VesselTemperature;
equation
  der(T) = q;
end VesselTemperature;