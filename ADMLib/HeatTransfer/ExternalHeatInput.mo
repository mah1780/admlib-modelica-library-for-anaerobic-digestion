within ADMLib.HeatTransfer;

model ExternalHeatInput
  extends ADMLib.Interfaces.InOutHeatProcess;
  Modelica.Blocks.Interfaces.RealInput qin;
equation
  q = qin;
end ExternalHeatInput;