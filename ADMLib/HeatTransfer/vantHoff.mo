within ADMLib.HeatTransfer;

model vantHoff
  ADMLib.Types.Constant K;
  ADMLib.Types.Constant a;
  ADMLib.Types.Temperature b;
  ADMLib.Types.Temperature Tbase;
  ADMLib.Types.Temperature Top;
equation
  K = a * exp(b * (1 / Tbase - 1 / Top));
end vantHoff;