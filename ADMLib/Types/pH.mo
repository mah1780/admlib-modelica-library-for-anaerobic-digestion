within ADMLib.Types;

type pH = Real(final quantity = "pH", min = 0);