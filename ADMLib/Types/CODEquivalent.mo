within ADMLib.Types;

type CODEquivalent = Real(final quantity = "COD equivalent");