within ADMLib.Types;

type NitrogenContents = Real(final quantity = "Nitrogen contents", min = 0);