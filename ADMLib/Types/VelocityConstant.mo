within ADMLib.Types;

type VelocityConstant = Real(final quantity = "Velocity constant", min = 0);