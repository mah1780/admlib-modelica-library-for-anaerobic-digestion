within ADMLib.Types;

type PipeCoefficient = Real(final quantity = "Pipe friction coefficient", min = 0);