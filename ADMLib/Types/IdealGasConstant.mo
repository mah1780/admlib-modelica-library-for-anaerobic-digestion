within ADMLib.Types;

type IdealGasConstant = Real(final quantity = "Ideal gas constant", min = 0);