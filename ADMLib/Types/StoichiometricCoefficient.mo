within ADMLib.Types;

type StoichiometricCoefficient = Real(final quantity = "Stoichiometric coefficient", min = 0);