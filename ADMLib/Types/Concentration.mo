within ADMLib.Types;

type Concentration = Real(final quantity = "Concentration", min = 0);