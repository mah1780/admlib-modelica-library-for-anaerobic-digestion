within ADMLib.Interfaces;

partial model PThreeP
  ProcessPort R, P1, P2, P3;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient frr, fpp1, fpp2, fpp3;
equation
  R.r = r * frr;
  P1.r = -r * fpp1;
  P2.r = -r * fpp2;
  P3.r = -r * fpp3;
end PThreeP;