within ADMLib;
package Interfaces "A package for connectors and partial models"
  extends Modelica.Icons.Package;
  
  connector ProcessPort "Connects substances and processes"
  ADMLib.Types.Concentration c;
  flow ADMLib.Types.ProcessRate r;
  annotation(
      Icon(graphics = {Ellipse(origin = {-2, 1}, lineColor = {3, 22, 1}, fillColor = {46, 91, 8}, fillPattern = FillPattern.Sphere, lineThickness = 1, extent = {{-58, 59}, {60, -59}}, endAngle = 360)}));
  end ProcessPort;

  connector TemperaturePort "Connects temperatures and heat processes"
  ADMLib.Types.Temperature T;
  flow ADMLib.Types.HeatRate q;
  annotation(
      Icon(graphics = {Ellipse(origin = {-2, 1}, lineColor = {3, 22, 1}, fillColor = {164, 0, 0}, fillPattern = FillPattern.Sphere, lineThickness = 1, extent = {{-58, 59}, {60, -59}}, endAngle = 360)}, coordinateSystem(initialScale = 0.1)));
  end TemperaturePort;
  
  connector ValuePort "Connects values and processes"
  ADMLib.Types.Value v;
  flow ADMLib.Types.Term St;
  annotation(
      Icon(graphics = {Ellipse(origin = {-2, 1}, lineColor = {3, 22, 1}, fillColor = {196, 160, 0}, fillPattern = FillPattern.Sphere, lineThickness = 1, extent = {{-58, 59}, {60, -59}}, endAngle = 360)}, coordinateSystem(initialScale = 0.1)));
  end ValuePort;
  
  partial model Process "One reactant, one product process"
  ProcessPort R,P;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr,fpp;
  equation
  R.r = r * frr; P.r = -r * fpp;
  end Process;
  
  partial model PTwoP "One reactant, two products process"
  ProcessPort R,P1,P2;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr,fpp1,fpp2;
  equation
  R.r = r * frr; P1.r = -r * fpp1; P2.r = -r * fpp2;
  end PTwoP;

  partial model PThreeP "One reactant, three products process"
  ProcessPort R,P1,P2,P3;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr,fpp1,fpp2,fpp3;
  equation
  R.r = r * frr; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3;
  end PThreeP;
  
  partial model PFourP "One reactant, four products process"
  ProcessPort R,P1,P2,P3,P4;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr,fpp1,fpp2,fpp3,fpp4;
  equation
  R.r = r * frr; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4;
  end PFourP;
  
  partial model PFiveP "One reactant, five products process"
  ProcessPort R,P1,P2,P3,P4,P5;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr,fpp1,fpp2,fpp3,fpp4,fpp5;
  equation
  R.r = r * frr; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5;
  end PFiveP;
  
  partial model PSixP "One reactant, six products process"
  ProcessPort R,P1,P2,P3,P4,P5,P6;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr,fpp1,fpp2,fpp3,fpp4,fpp5,fpp6;
  equation
  R.r = r * frr; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5; P6.r = -r * fpp6;
  end PSixP;
  
  partial model PTwoR "Two reactants, one product process"
  ProcessPort R1,R2,P;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp;
  equation
  R1.r = r * frr1; R2.r = r * frr2; P.r = -r * fpp;
  end PTwoR;  
  
  partial model PTwoRTwoP "Two reactants, two products process"
  ProcessPort R1,R2,P1,P2;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp1,fpp2;
  equation
  R1.r = r * frr1; R2.r = r * frr2; P1.r = -r * fpp1; P2.r = -r * fpp2;
  end PTwoRTwoP; 
  
  partial model PTwoRThreeP "Two reactants, three products process"
  ProcessPort R1,R2,P1,P2,P3;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp1,fpp2,fpp3;
  equation
  R1.r = r * frr1; R2.r = r * frr2; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3;
  end PTwoRThreeP; 
  
  partial model PTwoRFourP "Two reactants, four products process"
  ProcessPort R1,R2,P1,P2,P3,P4;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp1,fpp2,fpp3,fpp4;
  equation
  R1.r = r * frr1; R2.r = r * frr2; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4;
  end PTwoRFourP; 
  
  partial model PTwoRFiveP "Two reactants, five products process"
  ProcessPort R1,R2,P1,P2,P3,P4,P5;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp1,fpp2,fpp3,fpp4,fpp5;
  equation
  R1.r = r * frr1; R2.r = r * frr2; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5;
  end PTwoRFiveP;
  
  partial model PTwoRSixP "Two reactants, six products process"
  ProcessPort R1,R2,P1,P2,P3,P4,P5,P6;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp1,fpp2,fpp3,fpp4,fpp5,fpp6;
  equation
  R1.r = r * frr1; R2.r = r * frr2; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5; P6.r = -r * fpp6;
  end PTwoRSixP;
  
  partial model PThreeR "Three reactants, one product process"
  ProcessPort R1,R2,R3,P;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P.r = -r * fpp;
  end PThreeR; 
  
  partial model PThreeRTwoP "Three reactants, two products process"
  ProcessPort R1,R2,R3,P1,P2;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp1,fpp2;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P1.r = -r * fpp1; P2.r = -r * fpp2;
  end PThreeRTwoP; 
  
  partial model PThreeRThreeP "Three reactants, three products process"
  ProcessPort R1,R2,R3,P1,P2,P3;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp1,fpp2,fpp3;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3;
  end PThreeRThreeP; 
  
  partial model PThreeRFourP "Three reactants, four products process"
  ProcessPort R1,R2,R3,P1,P2,P3,P4;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp1,fpp2,fpp3,fpp4;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4;
  end PThreeRFourP; 

  partial model PThreeRFiveP "Three reactants, five products process"
  ProcessPort R1,R2,R3,P1,P2,P3,P4,P5;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp1,fpp2,fpp3,fpp4,fpp5;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5;
  end PThreeRFiveP;
  
  partial model PThreeRSixP "Three reactants, six products process"
  ProcessPort R1,R2,R3,P1,P2,P3,P4,P5,P6;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp1,fpp2,fpp3,fpp4,fpp5,fpp6;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5; P6.r = -r *fpp6;
  end PThreeRSixP;
 
  partial model BioProcess "One reactant, one product, one substrate, biochemical process"
  ProcessPort R,P,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr,fpp,fbb;
  equation
  R.r = r * frr; P.r = -r * fpp; B.r = -r * fbb;
  end BioProcess;
  
  partial model BPTwoP "One reactant, two products, one substrate, biochemical process"
  ProcessPort R,P1,P2,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr,fpp1,fpp2,fbb;
  equation
  R.r = r * frr; P1.r = -r * fpp1; P2.r = -r * fpp2; B.r = -r * fbb;
  end BPTwoP;
  
  partial model BPThreeP "One reactant, three products, one substrate, biochemical process"
  ProcessPort R,P1,P2,P3,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr,fpp1,fpp2,fpp3,fbb;
  equation
  R.r = r * frr; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; B.r = -r * fbb;
  end BPThreeP;
  
  partial model BPFourP "One reactant, four products, one substrate, biochemical process"
  ProcessPort R,P1,P2,P3,P4,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr,fpp1,fpp2,fpp3,fpp4,fbb;
  equation
  R.r = r * frr; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; B.r = -r * fbb;
  end BPFourP;
  
  partial model BPFiveP "One reactant, five products, one substrate, biochemical process"
  ProcessPort R,P1,P2,P3,P4,P5,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr,fpp1,fpp2,fpp3,fpp4,fpp5,fbb;
  equation
  R.r = r * frr; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5; B.r = -r * fbb;
  end BPFiveP;
  
  partial model BPSixP "One reactant, six products, one substrate, biochemical process"
  ProcessPort R,P1,P2,P3,P4,P5,P6,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr,fpp1,fpp2,fpp3,fpp4,fpp5,fpp6,fbb;
  equation
  R.r = r * frr; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5; P6.r = -r * fpp6; B.r = -r * fbb;
  end BPSixP;
  
  partial model BPTwoR "Two reactants, one product, one substrate, biochemical process"
  ProcessPort R1,R2,P,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp,fbb;
  equation
  R1.r = r * frr1; R2.r = -r * frr2; P.r = -r * fpp; B.r = -r * fbb;
  end BPTwoR;
  
  partial model BPTwoRTwoP "Two reactants, two products, one substrate, biochemical process"
  ProcessPort R1, R2, P1, P2,P3,P4,P5,P6, B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp1,fpp2,fpp3,fpp4,fpp5,fpp6,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2;   P1.r = -r * fpp1; P2.r = -r * fpp2; B.r = -r * fbb;
  end BPTwoRTwoP;
  
  partial model BPTwoRThreeP "Two reactants, three products, one substrate, biochemical process"
  ProcessPort R1,R2,P1,P2,P3,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp1,fpp2,fpp3,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2;   P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; B.r = -r * fbb;
  end BPTwoRThreeP;
  
  partial model BPTwoRFourP "Two reactants, four products, one substrate, biochemical process"
  ProcessPort R1,R2,P1,P2,P3,P4,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp1,fpp2,fpp3,fpp4,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2;   P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; B.r = -r * fbb;
  end BPTwoRFourP;
  
  partial model BPTwoRFiveP "Two reactants, five products, one substrate, biochemical process"
  ProcessPort R1,R2,P1,P2,P3,P4,P5,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp1,fpp2,fpp3,fpp4,fpp5,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2;   P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5; B.r = -r * fbb;
  end BPTwoRFiveP;
  
  partial model BPTwoRSixP "Two reactants, six products, one substrate, biochemical process"
  ProcessPort R1, R2, P1, P2,P3,P4,P5,P6, B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp1,fpp2,fpp3,fpp4,fpp5,fpp6,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2;   P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5; P6.r = -r * fpp6; B.r = -r * fbb;
  end BPTwoRSixP;
  
  partial model BPThreeR "Three reactants, one product, one substrate, biochemical process"
  ProcessPort R1,R2,R3,P,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P.r = -r * fpp; B.r = -r * fbb;
  end BPThreeR;

  partial model BPThreeRTwoP "Three reactants, two products, one substrate, biochemical process"
  ProcessPort R1,R2,R3,P1,P2,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp1,fpp2,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P1.r = -r * fpp1; P2.r = -r * fpp2; B.r = -r * fbb;
  end BPThreeRTwoP;

  partial model BPThreeRThreeP "Three reactants, three products, one substrate, biochemical process"
  ProcessPort R1,R2,R3,P1,P2,P3,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp1,fpp2,fpp3,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; B.r = -r * fbb;
  end BPThreeRThreeP;

  partial model BPThreeRFourP "Three reactants, four products, one substrate, biochemical process"
  ProcessPort R1, R2, R3, P1, P2, P3, P4, B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp1,fpp2,fpp3,fpp4,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; B.r = -r * fbb;
  end BPThreeRFourP;

  partial model BPThreeRFiveP "Three reactants, five products, one substrate, biochemical process"
  ProcessPort R1, R2, R3, P1, P2, P3, P4, P5, B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp1,fpp2,fpp3,fpp4,fpp5,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5; B.r = -r * fbb;
  end BPThreeRFiveP;
  
  partial model BPThreeRSixP "Three reactants, six products, one substrate, biochemical process"
  ProcessPort R1,R2,R3,P1,P2,P3,P4,P5,P6,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp1,fpp2,fpp3,fpp4,fpp5,fpp6,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5; P6.r = -r * fpp6; B.r = -r * fbb;
  end BPThreeRSixP;
  
  partial model BPTwoS "One-two reactants, one product, two substrates, biochemical process"
  ProcessPort R1,R2,P,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; P.r = -r * fpp; B.r = -r * fbb;
  end BPTwoS;
  
  partial model BPTwoSTwoP "One-two reactants, two products, two substrates, biochemical process"
  ProcessPort R1,R2,P1,P2,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp1,fpp2,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; P1.r = -r * fpp1; P2.r = -r * fpp2; B.r = -r * fbb;
  end BPTwoSTwoP;
  
  partial model BPTwoSThreeP "One-two reactants, three products, two substrates, biochemical process"
  ProcessPort R1,R2,P1,P2,P3,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp1,fpp2,fpp3,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; B.r = -r * fbb;
  end BPTwoSThreeP;
  
  partial model BPTwoSFourP "One-two reactants, four products, two substrates, biochemical process"
  ProcessPort R1,R2,P1,P2,P3,P4,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp1,fpp2,fpp3,fpp4,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; B.r = -r * fbb;
  end BPTwoSFourP;
  
  partial model BPTwoSFiveP "One-two reactants, five products, two substrates, biochemical process"
  ProcessPort R1,R2,P1,P2,P3,P4,P5,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp1,fpp2,fpp3,fpp4,fpp5,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5; B.r = -r * fbb;
  end BPTwoSFiveP;
  
  partial model BPTwoSSixP "One-two reactants, six products, two substrates, biochemical process"
  ProcessPort R1,R2,P1,P2,P3,P4,P5,P6,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,fpp1,fpp2,fpp3,fpp4,fpp5,fpp6,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5; P6.r = -r * fpp6; B.r = -r * fbb;
  end BPTwoSSixP;
  
  partial model BPTwoSThreeR "Three reactants, one product, two substrates, biochemical process"
  ProcessPort R1,R2,R3,P,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P.r = -r * fpp; B.r = -r * fbb;
  end BPTwoSThreeR;
  
  partial model BPTwoSThreeRTwoP "Three reactants, two products, two substrates, biochemical process"
  ProcessPort R1,R2,R3,P1,P2,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp1,fpp2,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P1.r = -r * fpp1; P2.r = -r * fpp2; B.r = -r * fbb;
  end BPTwoSThreeRTwoP;
  
  partial model BPTwoSThreeRThreeP "Three reactants, three products, two substrates, biochemical process"
  ProcessPort R1,R2,R3,P1,P2,P3,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp1,fpp2,fpp3,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; B.r = -r * fbb;
  end BPTwoSThreeRThreeP;
  
  partial model BPTwoSThreeRFourP "Three reactants, four products, two substrates, biochemical process"
  ProcessPort R1,R2,R3,P1,P2,P3,P4,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp1,fpp2,fpp3,fpp4,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; B.r = -r * fbb;
  end BPTwoSThreeRFourP;
  
  partial model BPTwoSThreeRFiveP "Three reactants, five products, two substrates, biochemical process"
  ProcessPort R1,R2,R3,P1,P2,P3,P4,P5,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp1,fpp2,fpp3,fpp4,fpp5,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5; B.r = -r * fbb;
  end BPTwoSThreeRFiveP;
  
  partial model BPTwoSThreeRSixP "Three reactants, six products, two substrates, biochemical process"
  ProcessPort R1,R2,R3,P1,P2,P3,P4,P5,P6,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,fpp1,fpp2,fpp3,fpp4,fpp5,fpp6,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5; P6.r = -r * fpp6; B.r = -r * fbb;
  end BPTwoSThreeRSixP;
  
  partial model BPTwoSFourR "Four reactants, one product, two substrates, biochemical process"
  ProcessPort R1,R2,R3,R4,P,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,frr4,fpp,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; R4.r = r * frr4; P.r = -r * fpp; B.r = -r * fbb;
  end BPTwoSFourR;
  
  partial model BPTwoSFourRTwoP "Four reactants, two products, two substrates, biochemical process"
  ProcessPort R1,R2,R3,R4,P1,P2,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,frr4,fpp1,fpp2,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; R4.r = r * frr4; P1.r = -r * fpp1; P2.r = -r * fpp2; B.r = -r * fbb;
  end BPTwoSFourRTwoP;

  partial model BPTwoSFourRThreeP "Four reactants, three products, two substrates, biochemical process"
  ProcessPort R1,R2,R3,R4,P1,P2,P3,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,frr4,fpp1,fpp2,fpp3,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; R4.r = r * frr4; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; B.r = -r * fbb;
  end BPTwoSFourRThreeP;
  
  partial model BPTwoSFourRFourP "Four reactants, four products, two substrates, biochemical process"
  ProcessPort R1,R2,R3,R4,P1,P2,P3,P4,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,frr4,fpp1,fpp2,fpp3,fpp4,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; R4.r = r * frr4; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; B.r = -r * fbb;
  end BPTwoSFourRFourP;

  partial model BPTwoSFourRFiveP "Four reactants, five products, two substrates, biochemical process"
  ProcessPort R1,R2,R3,R4,P1,P2,P3,P4,P5,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,frr4,fpp1,fpp2,fpp3,fpp4,fpp5,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; R4.r = r * frr4; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5; B.r = -r * fbb;
  end BPTwoSFourRFiveP;

  partial model BPTwoSFourRSixP "Four reactants, six products, two substrates, biochemical process"
  ProcessPort R1,R2,R3,R4,P1,P2,P3,P4,P5,P6,B;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient frr1,frr2,frr3,frr4,fpp1,fpp2,fpp3,fpp4,fpp5,fpp6,fbb;
  equation
  R1.r = r * frr1; R2.r = r * frr2; R3.r = r * frr3; R4.r = r * frr4; P1.r = -r * fpp1; P2.r = -r * fpp2; P3.r = -r * fpp3; P4.r = -r * fpp4; P5.r = -r * fpp5; P6.r = -r * fpp6; B.r = -r * fbb;
  end BPTwoSFourRSixP;

  partial model InOutProcess "Input/Output process"
  ProcessPort S;
  ADMLib.Types.ProcessRate r;
  equation
  S.r = -r;
  end InOutProcess;

  partial model Substance "General behaviour of a substance"
  ProcessPort S;
  ADMLib.Types.Concentration c; ADMLib.Types.ProcessRate r;
  equation
  S.r = r; S.c = c;
  end Substance;
  
  partial model Value "A calculated value"
  ValuePort V;
  ADMLib.Types.Value v; ADMLib.Types.Term St;
  equation
  V.v = v; V.St = St;
  end Value;

  partial model VesselTemperature "General behaviour of a vessel temperature"
  TemperaturePort H;
  ADMLib.Types.Temperature T; ADMLib.Types.HeatRate q;
  equation
  H.T = T; H.q = q;
  end VesselTemperature;

  partial model InOutHeatProcess "Input/Output heating process"
  TemperaturePort H;
  ADMLib.Types.HeatRate q;
  equation
  H.q = -q;
  end InOutHeatProcess;
  
  partial model AcidBaseProcess "Acid-base interaction process"
  ProcessPort An;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient faa;
  equation
  An.r = faa*r;
  end AcidBaseProcess;
  
  partial model AcidBaseEquilibriumProcess "Acid-base equilibrium process"
  ValuePort AB;
  ADMLib.Types.Term St; ADMLib.Types.StoichiometricCoefficient feeq;
  equation
  AB.St = -feeq*St;
  end AcidBaseEquilibriumProcess;
  
  partial model HydroniumProcess "Hydronium concentration representing process"
  ProcessPort S;  
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient fhh;
  equation
  S.r = -fhh*r;
  end HydroniumProcess;
  
  partial model LiquidGasProcess "Liquid-gas interaction process"
  ProcessPort L, G;
  ADMLib.Types.ProcessRate r; ADMLib.Types.StoichiometricCoefficient fll,fgg;
  equation
  L.r = fll*r; G.r = -fgg*r;
  end LiquidGasProcess;
 
  annotation(
    Icon(graphics = {Line(origin = {-133.397, 80.4253}, points = {{51.135, -26}, {52.135, -63}, {97.135, -64}}, pattern = LinePattern.Dot, thickness = 1, arrow = {Arrow.None, Arrow.Filled}, arrowSize = 15, smooth = Smooth.Bezier), Rectangle(origin = {21, -76}, lineColor = {65, 255, 0}, fillColor = {3, 22, 1}, fillPattern = FillPattern.Cross, lineThickness = 0.75, extent = {{-55, 114}, {17, 42}}), Line(origin = {-25.61, -77.361}, rotation = 180, points = {{55.135, -24}, {52.135, -61}, {11.135, -60}}, pattern = LinePattern.Dot, thickness = 1, arrow = {Arrow.None, Arrow.Filled}, arrowSize = 15, smooth = Smooth.Bezier), Line(origin = {13.3972, -33.5441}, rotation = 90, points = {{51.135, -26}, {52.135, -63}, {97.135, -64}}, pattern = LinePattern.Dot, thickness = 1, arrow = {Arrow.None, Arrow.Filled}, arrowSize = 15, smooth = Smooth.Bezier), Line(origin = {16.451, -73.7732}, rotation = 90, points = {{55.135, -24}, {52.135, -61}, {11.135, -60}}, pattern = LinePattern.Dot, thickness = 1, arrow = {Arrow.None, Arrow.Filled}, arrowSize = 15, smooth = Smooth.Bezier)}, coordinateSystem(initialScale = 0.1)));
end Interfaces;