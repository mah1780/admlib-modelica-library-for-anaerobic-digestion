within ADMLib.Interfaces;

partial model InOutProcess
  ProcessPort S;
  ADMLib.Types.ProcessRate r;
equation
  S.r = -r;
end InOutProcess;