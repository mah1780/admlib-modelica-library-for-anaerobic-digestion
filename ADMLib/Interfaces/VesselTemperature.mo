within ADMLib.Interfaces;

partial model VesselTemperature
  TemperaturePort H;
  ADMLib.Types.Temperature T;
  ADMLib.Types.HeatRate q;
equation
  H.T = T;
  H.q = q;
end VesselTemperature;