within ADMLib.Interfaces;

partial model PThreeR "Three reactants, one product, non-biochemical process"
  ProcessPort R1, R2, R3, P;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient frr1, frr2, frr3, fpp;
equation
  R1.r = r * frr1;
  R2.r = r * frr2;
  R3.r = r * frr3;
  P.r = -r * fpp;
end PThreeR;