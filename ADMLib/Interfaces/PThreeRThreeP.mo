within ADMLib.Interfaces;

partial model PThreeRThreeP "Three reactants, three products, non-biochemical process"
  ProcessPort R1, R2, R3, P1, P2, P3;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient frr1, frr2, frr3, fpp1, fpp2, fpp3;
equation
  R1.r = r * frr1;
  R2.r = r * frr2;
  R3.r = r * frr3;
  P1.r = -r * fpp1;
  P2.r = -r * fpp2;
  P3.r = -r * fpp3;
end PThreeRThreeP;