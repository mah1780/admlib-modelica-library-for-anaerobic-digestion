within ADMLib.Interfaces;

partial model HydroniumProcess
  ProcessPort S;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient fhhq;
equation
  AB.r = -fhhq * r;
end HydroniumProcess;