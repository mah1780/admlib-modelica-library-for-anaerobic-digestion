within ADMLib.Interfaces;

partial model BPTwoP
  ProcessPort R, P1, P2, B;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient frr, fpp1, fpp2, fbb;
equation
  R.r = r * frr;
  P1.r = -r * fpp1;
  P2.r = -r * fpp2;
  B.r = -r * fbb;
end BPTwoP;