within ADMLib.Interfaces;

partial model BPTwoSTwoP
  ProcessPort R1, R2, P1, P2, B;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient frr1, frr2, fpp1, fpp2, fbb;
equation
  R1.r = r * frr1;
  R2.r = r * frr2;
  P1.r = -r * fpp1;
  P2.r = -r * fpp2;
  B.r = -r * fbb;
end BPTwoSTwoP;