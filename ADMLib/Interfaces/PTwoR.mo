within ADMLib.Interfaces;

partial model PTwoR
  ProcessPort R1, R2, P;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient frr1, frr2, fpp;
equation
  R1.r = r * frr1;
  R2.r = r * frr2;
  P.r = -r * fpp;
end PTwoR;