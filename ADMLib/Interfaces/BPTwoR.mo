within ADMLib.Interfaces;

partial model BPTwoR
  ProcessPort R1, R2, P, B;
  ADMLib.Types.ProcessRate r;
  ADMLib.Types.StoichiometricCoefficient frr1, frr2, fpp, fbb;
equation
  R1.r = r * frr1;
  R2.r = -r * frr2;
  P.r = -r * fpp;
  B.r = -r * fbb;
end BPTwoR;