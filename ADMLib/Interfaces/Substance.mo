within ADMLib.Interfaces;

partial model Substance
  ProcessPort S;
  ADMLib.Types.Concentration c;
  ADMLib.Types.ProcessRate r;
equation
  S.r = r;
  S.c = c;
end Substance;