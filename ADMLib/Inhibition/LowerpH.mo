within ADMLib.Inhibition;

model LowerpH
  ADMLib.Types.InhibitionFunction If;
  ADMLib.Types.pH pH;
  ADMLib.Types.pH pHLL = 1;
  ADMLib.Types.pH pHUL = 1;
equation
  if pH < pHUL then
    If = exp(-3 * ((pH - pHUL) / (pHUL - pHLL)) ^ 2);
  else
    If = 1;
  end if;
end LowerpH;