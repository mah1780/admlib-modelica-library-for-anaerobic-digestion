within ADMLib.MassBalance.FirstOrder;

model FOPThreeP
  extends ADMLib.Interfaces.PThreeP;
  ADMLib.Types.VelocityConstant k;
  ADMLib.Types.StoichiometricCoefficient fr;
  ADMLib.Types.StoichiometricCoefficient fp1;
  ADMLib.Types.StoichiometricCoefficient fp2;
  ADMLib.Types.StoichiometricCoefficient fp3;
equation
  r = k * R.c;
  frr = fr;
  fpp1 = fp1;
  fpp2 = fp2;
  fpp3 = fp3;
end FOPThreeP;