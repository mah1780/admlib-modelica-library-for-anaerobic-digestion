within ADMLib.MassBalance.InletOutlet;

model InOut
  extends ADMLib.Interfaces.InOutProcess;
  ADMLib.Types.Volume V;
  ADMLib.Types.VolumetricFlow Q;
  Modelica.Blocks.Interfaces.RealInput Sin;
equation
  r = Q / V * (Sin - S.c);
end InOut;