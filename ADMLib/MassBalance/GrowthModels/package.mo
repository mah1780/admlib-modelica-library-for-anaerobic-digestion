within ADMLib.MassBalance;
package GrowthModels "A subpackage to describe biochemical reactions with different kinetics"
  extends Modelica.Icons.Package;
  
  model Monod "One reactant, one product, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BioProcess;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr,fp,fb;
  equation
  r = ((kmax * R.c)/(Ks + R.c)) * B.c * product(I); frr = fr; fpp = fp; fbb = fb;
  end Monod;
  
  model MTwoP "One reactant, two products, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoP; ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks;
  constant Integer dim;
  ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr,fp1,fp2,fb;
  equation
  r = ((kmax * R.c)/(Ks + R.c)) * B.c * product(I); frr = fr; fpp1 = fp1; fpp2 = fp2; fbb = fb;
  end MTwoP;
 
  model MThreeP "One reactant, three products, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPThreeP; ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks;
  constant Integer dim;
  ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr,fp1,fp2,fp3,fb;
  equation
  r = ((kmax * R.c)/(Ks + R.c)) * B.c * product(I); frr = fr; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fbb = fb;
  end MThreeP;

  model MFourP "One reactant, four products, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPFourP; ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks;
  constant Integer dim;
  ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr,fp1,fp2,fp3,fp4,fb;
  equation
  r = ((kmax * R.c)/(Ks + R.c)) * B.c * product(I); frr = fr; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fbb = fb;
  end MFourP;
  
  model MFiveP "One reactant, five products, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPFiveP; ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks;
  constant Integer dim;
  ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr,fp1,fp2,fp3,fp4,fp5,fb;
  equation
  r = ((kmax * R.c)/(Ks + R.c)) * B.c * product(I); frr = fr; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5; fbb = fb;
  end MFiveP;  
  
  model MSixP "One reactant, six products, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPFiveP; ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks;
  constant Integer dim;
  ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr,fp1,fp2,fp3,fp4,fp5,fp6,fb;
  equation
  r = ((kmax * R.c)/(Ks + R.c)) * B.c * product(I); frr = fr; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5; fpp6 = fp6; fbb = fb;
  end MSixP; 
  
  model MTwoR "Two reactants, one product, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoR; ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks;
  constant Integer dim;
  ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; fpp = fp; fbb = fb;
  end MTwoR; 

  model MTwoRTwoP "Two reactants, two products, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoRTwoP; ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks;
  constant Integer dim;
  ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp1,fp2,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; fpp1 = fp1; fpp2 = fp2; fbb = fb;
  end MTwoRTwoP; 

  model MTwoRThreeP "Two reactants, three products, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoRThreeP; ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks;
  constant Integer dim;
  ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp1,fp2,fp3,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fbb = fb;
  end MTwoRThreeP; 
  
  model MTwoRFourP "Two reactants, four products, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoRFourP; ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks;
  constant Integer dim;
  ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp1,fp2,fp3,fp4,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fbb = fb;
  end MTwoRFourP;

  model MTwoRFiveP "Two reactants, five products, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoRFiveP; ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks;
  constant Integer dim;
  ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp1,fp2,fp3,fp4,fp5,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5; fbb = fb;
  end MTwoRFiveP;
  
  model MTwoRSixP "Two reactants, six products, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoRSixP; ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks;
  constant Integer dim;
  ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp1,fp2,fp3,fp4,fp5,fp6,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5; fpp6 = fp6; fbb = fb;
  end MTwoRSixP;
  
  model MThreeR "Three reactants, one product, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPThreeR;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * B.c*product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp = fp; fbb = fb;
  end MThreeR;
  
  model MThreeRTwoP "Three reactants, two products, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPThreeRTwoP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks;
  ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp1,fp2,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * B.c *product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp1 = fp1; fpp2 = fp2; fbb = fb;
  end MThreeRTwoP;
  
  model MThreeRThreeP "Three reactants, three products, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPThreeRThreeP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp1,fp2,fp3,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * B.c *product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fbb = fb;
  end MThreeRThreeP;  
  
  model MThreeRFourP "Three reactants, four products, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPThreeRFourP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp1,fp2,fp3,fp4,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * B.c *product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fbb = fb; 
  end MThreeRFourP;

  model MThreeRFiveP "Three reactants, five products, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPThreeRFiveP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp1,fp2,fp3,fp4,fp5,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * B.c *product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5; fbb = fb; 
  end MThreeRFiveP;

  model MThreeRSixP "Three reactants, six products, one substrate, Monod kinetics"
  extends ADMLib.Interfaces.BPThreeRSixP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp1,fp2,fp3,fp4,fp5,fp6,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * B.c *product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5; fpp6 = fp6; fbb = fb; 
  end MThreeRSixP;
  
  model MTwoS "One-two reactants, one product, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoS;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; fpp = fp; fbb = fb;
  end MTwoS;
  
  model MTwoSTwoP "One-two reactants, two products, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSTwoP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp1,fp2,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; fpp1 = fp1; fpp2 = fp2; fbb = fb;
  end MTwoSTwoP;

  model MTwoSThreeP "One-two reactants, three products, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSThreeP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp1,fp2,fp3,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fbb = fb;
  end MTwoSThreeP;

  model MTwoSFourP "One-two reactants, four products, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSFourP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp1,fp2,fp3,fp4,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fbb = fb;
  end MTwoSFourP;

  model MTwoSFiveP "One-two reactants, five products, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSFiveP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp1,fp2,fp3,fp4,fp5,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5; fbb = fb;
  end MTwoSFiveP;
  
  model MTwoSSixP "One-two reactants, six products, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSSixP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fp1,fp2,fp3,fp4,fp5,fp6,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5; fpp6 = fp6; fbb = fb;
  end MTwoSSixP;

  model MTwoSThreeR "Three reactants, one product, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSThreeR;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp = fp; fbb = fb;
  end MTwoSThreeR;

  model MTwoSThreeRTwoP "Three reactants, two products, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSThreeRTwoP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp1,fp2,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp1 = fp1; fpp2 = fp2; fbb = fb;
  end MTwoSThreeRTwoP;
  
  model MTwoSThreeRThreeP "Three reactants, three products, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSThreeRThreeP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp1,fp2,fp3,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fbb = fb;
  end MTwoSThreeRThreeP;
  
  model MTwoSThreeRFourP "Three reactants, four products, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSThreeRFourP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp1,fp2,fp3,fp4,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fbb = fb;
  end MTwoSThreeRFourP;
  
  model MTwoSThreeRFiveP "Three reactants, five products, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSThreeRFiveP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp1,fp2,fp3,fp4,fp5,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5; fbb = fb;
  end MTwoSThreeRFiveP;

  model MTwoSThreeRSixP "Three reactants, six products, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSThreeRSixP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fp1,fp2,fp3,fp4,fp5,fp6,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5; fpp6 = fp6; fbb = fb;
  end MTwoSThreeRSixP;

  model MTwoSFourR "Four reactants, one product, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSFourR;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fr4,fp,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; frr4 = fr4; fpp = fp; fbb = fb;
  end MTwoSFourR;
 
  model MTwoSFourRTwoP "Four reactants, two products, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSFourRTwoP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fr4,fp1,fp2,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; frr4 = fr4; fpp1 = fp1; fpp2 = fp2; fbb = fb;
  end MTwoSFourRTwoP;
 
  model MTwoSFourRThreeP "Four reactants, three products, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSFourRThreeP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fr4,fp1,fp2,fp3,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; frr4 = fr4; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fbb = fb;
  end MTwoSFourRThreeP;
 
  model MTwoSFourRFourP "Four reactants, four products, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSFourRFourP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fr4,fp1,fp2,fp3,fp4,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; frr4 = fr4; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fbb = fb;
  end MTwoSFourRFourP;
 
  model MTwoSFourRFiveP "Four reactants, five products, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSFourRFiveP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fr4,fp1,fp2,fp3,fp4,fp5,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; frr4 = fr4; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5; fbb = fb;
  end MTwoSFourRFiveP; 
 
  model MTwoSFourRSixP "Four reactants, six products, two substrates, Monod kinetics"
  extends ADMLib.Interfaces.BPTwoSFourRSixP;
  constant Integer dim;
  ADMLib.Types.VelocityConstant kmax; ADMLib.Types.Concentration Ks; ADMLib.Types.InhibitionFactor I[dim]; ADMLib.Types.StoichiometricCoefficient fr1,fr2,fr3,fr4,fp1,fp2,fp3,fp4,fp5,fp6,fb;
  equation
  r = ((kmax * R1.c)/(Ks + R1.c)) * ((R1.c)/(R1.c + R2.c)) * B.c * product(I); frr1 = fr1; frr2 = fr2; frr3 = fr3; frr4 = fr4; fpp1 = fp1; fpp2 = fp2; fpp3 = fp3; fpp4 = fp4; fpp5 = fp5; fpp6 = fp6; fbb = fb;
  end MTwoSFourRSixP;
 
  model Haldane "One reactant, one product, one substrate, Haldane kinetics"
  extends ADMLib.Interfaces.BioProcess;
  ADMLib.Types.VelocityConstant kmax;
  ADMLib.Types.Concentration Ks; ADMLib.Types.Concentration KI; ADMLib.Types.StoichiometricCoefficient fr,fp,fb;
  equation
  r = ((kmax * R.c)/(Ks + R.c + ((R.c)^2)/KI)) * B.c; frr = fr; fpp = fp; fbb = fb;
  end Haldane;
  
  model Moser "One reactant, one product, one substrate, Moser kinetics"
  extends ADMLib.Interfaces.BioProcess;
  ADMLib.Types.VelocityConstant kmax;
  ADMLib.Types.Concentration Ks; ADMLib.Types.StoichiometricCoefficient fr,fp,fb;
  constant Integer n;
  equation
  r = ((kmax * ((R.c)^n))/(Ks + ((R.c)^n))) * B.c; frr = fr; fpp = fp; fbb = fb;
  end Moser;
  
  model Contois "One reactant, one product, one substrate, Contois kinetics"
  extends ADMLib.Interfaces.BioProcess;
  ADMLib.Types.VelocityConstant kmax;
  ADMLib.Types.Constant Kc; ADMLib.Types.StoichiometricCoefficient fr,fp,fb;
  equation
  r = ((kmax * (R.c/B.c))/(Kc + (R.c/B.c))) * B.c; frr = fr; fpp = fp; fbb = fb;
  end Contois;
  
  model ChenHashimoto "One reactant, one product, one substrate, Chen and Hashimoto kinetics"
  extends ADMLib.Interfaces.BioProcess;
  ADMLib.Types.VelocityConstant kmax;
  ADMLib.Types.Concentration R0; ADMLib.Types.Constant KCH; ADMLib.Types.StoichiometricCoefficient fr,fp,fb;
  equation
  r = ((kmax * (R.c/R0))/((R.c/R0) + KCH*(1 - (R.c/R0)))) * B.c; frr = fr; fpp = fp; fbb = fb;
  end ChenHashimoto;
  
  model UncompetitiveInhibition "One reactant, one product, one substrate with uncompetitive inhibition included"
  extends ADMLib.Interfaces.BioProcess;
  ADMLib.Types.VelocityConstant kmax;
  ADMLib.Types.Concentration Ks; ADMLib.Types.Concentration KI; ADMLib.Types.Concentration SI; ADMLib.Types.StoichiometricCoefficient fr,fp,fb;
  equation
  r = ((kmax * R.c)/(Ks + R.c * (1 + (KI/SI)))) * B.c; frr = fr; fpp = fp; fbb = fb;
  end UncompetitiveInhibition;
  
  model CompetitiveInhibition "One reactant, one product, one substrate with competitive inhibition included"
  extends ADMLib.Interfaces.BioProcess;
  ADMLib.Types.VelocityConstant kmax;
  ADMLib.Types.Concentration Ks; ADMLib.Types.Concentration KI; ADMLib.Types.Concentration SI; ADMLib.Types.StoichiometricCoefficient fr,fp,fb;
  equation
  r = ((kmax * R.c)/(Ks * (1 + (SI/KI)) + R.c)) * B.c; frr = fr; fpp = fp; fbb = fb;
  end CompetitiveInhibition;
  
  annotation(
    Icon(graphics = {Line(origin = {-12.0897, 25.3871}, points = {{-10.865, -44}, {2.135, -20}, {29.135, 0}}, thickness = 0.75, arrow = {Arrow.None, Arrow.Filled}, arrowSize = 15, smooth = Smooth.Bezier), Ellipse(origin = {-43, -78}, lineColor = {143, 89, 2}, fillColor = {78, 154, 6}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, -2}}, endAngle = 360), Ellipse(origin = {-11, -56}, lineColor = {46, 52, 54}, fillColor = {114, 159, 207}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, 0}}, endAngle = 360), Ellipse(origin = {-49, -40}, lineColor = {46, 52, 54}, fillColor = {114, 159, 207}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, 0}}, endAngle = 360), Ellipse(origin = {23, 70}, lineColor = {143, 89, 2}, fillColor = {78, 154, 6}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, -2}}, endAngle = 360), Ellipse(origin = {81, 26}, lineColor = {143, 89, 2}, fillColor = {78, 154, 6}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, -2}}, endAngle = 360), Ellipse(origin = {55, 70}, lineColor = {46, 52, 54}, fillColor = {114, 159, 207}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, 0}}, endAngle = 360), Ellipse(origin = {87, 58}, lineColor = {46, 52, 54}, fillColor = {114, 159, 207}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, 0}}, endAngle = 360), Ellipse(origin = {45, 36}, lineColor = {46, 52, 54}, fillColor = {114, 159, 207}, pattern = LinePattern.Dot, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-23, 22}, {1, 0}}, endAngle = 360)}, coordinateSystem(initialScale = 0.1)));
end GrowthModels;
