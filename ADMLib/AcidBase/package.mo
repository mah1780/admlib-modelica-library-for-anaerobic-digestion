within ADMLib;
package AcidBase "A package for acid/base interactions and ion calculations"
  extends Modelica.Icons.Package;  
  
  model pH "Definition of pH"
  ADMLib.Types.Concentration CHplus; ADMLib.Types.pH pH;
  equation
  pH = -log10(CHplus);
  end pH;

  model Hydroxyl "Hydroxyl ion concentration equation"
  ADMLib.Types.Concentration CHplus; ADMLib.Types.Concentration COHminus; ADMLib.Types.Constant KW;
  equation
  COHminus = KW/CHplus;
  end Hydroxyl;

  model HydroniumConcentrationA "Hydronium ion concentration equation in the original ADM1"
  extends ADMLib.Interfaces.HydroniumProcess;
  ADMLib.Types.ProcessRate A; ADMLib.Types.Constant B; ADMLib.Types.StoichiometricCoefficient fh;
  equation
  r=A/B; fhh = fh;
  end HydroniumConcentrationA;

  model HydroniumConcentrationB "Hydronium ion concentration equation in the ADM1 benchmark implementation [Gernaey14]" 
  constant Integer dimCat = 1;
  constant Integer dimAn = 1;
  ADMLib.Types.CODEquivalent Cateq[dimCat]; ADMLib.Types.CODEquivalent Aneq[dimAn]; ADMLib.Types.Concentration Cations[dimCat]; ADMLib.Types.Concentration Anions[dimAn]; ADMLib.Types.Concentration Theta; ADMLib.Types.Concentration CHplus; ADMLib.Types.Constant KW;
  equation
  Theta = sum(Cateq.*Cations) - sum(Aneq.*Anions); CHplus = -(Theta/2)+(1/2)*sqrt((Theta^2)+4*KW);
  end HydroniumConcentrationB;

  model AcidBaseRate "Acid/base equilibrium process"
  extends ADMLib.Interfaces.AcidBaseProcess;
  ADMLib.Types.AcidBaseConstant kAB; ADMLib.Types.AcidBaseEquilibriumConstant Ka; ADMLib.Types.StoichiometricCoefficient fa; ADMLib.Types.Concentration CHplus; ADMLib.Types.Concentration C;
  equation
  r = kAB*(An.c*(Ka+CHplus) - Ka*C); faa = fa;
  end AcidBaseRate;
  
  model AcidBaseEquilibriumRateA "Term of the A factor in the original ADM1 hydronium ion concentration equation"
  extends ADMLib.Interfaces.AcidBaseEquilibriumProcess; 
  ADMLib.Types.AcidBaseEquilibriumConstant Ka; ADMLib.Types.StoichiometricCoefficient feq; ADMLib.Types.Concentration CHplus; ADMLib.Types.Concentration C;
  equation
  St = feq*(Ka/(Ka+CHplus))*der(C); feeq = feq;
  end AcidBaseEquilibriumRateA;
  
  model AcidBaseEquilibriumRateB "Term of the B factor in the original ADM1 hydronium ion concentration equation"
  extends ADMLib.Interfaces.AcidBaseEquilibriumProcess;
  ADMLib.Types.AcidBaseEquilibriumConstant Ka; ADMLib.Types.StoichiometricCoefficient feq; ADMLib.Types.Concentration CHplus; ADMLib.Types.Concentration C;
  equation
  St = feq*((Ka*C)/((Ka+CHplus)^2)); feeq = feq;
  end AcidBaseEquilibriumRateB;
  
  model AcidBaseEquilibriumRateH2O "Water term of the B factor in the original ADM1 hydronium ion concentration equation"
  extends ADMLib.Interfaces.AcidBaseEquilibriumProcess;
  ADMLib.Types.AcidBaseEquilibriumConstant Kw; ADMLib.Types.StoichiometricCoefficient feq; ADMLib.Types.Concentration CHplus;
  equation
  St = feq*(Kw/((CHplus)^2)); feeq = feq;
  end AcidBaseEquilibriumRateH2O;
  
  model IonPair "An anion and cation pair"
  ADMLib.Types.Concentration Ctot; ADMLib.Types.Concentration Ccat; ADMLib.Types.Concentration Can;
  equation
  Ctot = Ccat - Can;
  end IonPair;
  
  annotation(
    Icon(graphics = {Ellipse(origin = {-52, 70}, lineColor = {114, 159, 207}, fillColor = {32, 74, 135}, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-14, 14}, {14, -14}}, endAngle = 360), Ellipse(origin = {62, 42}, lineColor = {114, 159, 207}, fillColor = {32, 74, 135}, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-14, 14}, {14, -14}}, endAngle = 360), Ellipse(origin = {-14, -64}, lineColor = {114, 159, 207}, fillColor = {32, 74, 135}, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-14, 14}, {14, -14}}, endAngle = 360), Ellipse(origin = {38, -6}, lineColor = {114, 159, 207}, fillColor = {32, 74, 135}, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-14, 14}, {14, -14}}, endAngle = 360), Ellipse(origin = {-60, 14}, lineColor = {252, 175, 62}, fillColor = {204, 0, 0}, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-14, 14}, {6, -6}}, endAngle = 360), Ellipse(origin = {12, 58}, lineColor = {252, 175, 62}, fillColor = {204, 0, 0}, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-14, 14}, {6, -6}}, endAngle = 360), Ellipse(origin = {-18, -30}, lineColor = {252, 175, 62}, fillColor = {204, 0, 0}, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-14, 14}, {6, -6}}, endAngle = 360), Ellipse(origin = {82, -58}, lineColor = {252, 175, 62}, fillColor = {204, 0, 0}, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-14, 14}, {6, -6}}, endAngle = 360), Ellipse(origin = {-72, -76}, lineColor = {252, 175, 62}, fillColor = {204, 0, 0}, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-14, 14}, {6, -6}}, endAngle = 360), Line(origin = {-41.8702, 68.8855}, points = {{-15, 2}, {-5, 2}}, thickness = 1), Line(origin = {48.2824, -7.14507}, points = {{-15, 2}, {-5, 2}}, thickness = 1), Line(origin = {72.7099, 40.2595}, points = {{-15, 2}, {-5, 2}}, thickness = 1), Line(origin = {-3.32063, -66.2291}, points = {{-15, 2}, {-5, 2}}, thickness = 1), Line(origin = {86.987, -56.0787}, points = {{-12, 2}, {-5, 2}}, thickness = 1), Line(origin = {78.2901, -57.7404}, points = {{0, 8}, {0, 0}}, thickness = 0.75), Line(origin = {8.13743, 58.0611}, points = {{0, 8}, {0, 0}}, thickness = 0.75), Line(origin = {16.8343, 59.7228}, points = {{-12, 2}, {-5, 2}}, thickness = 1), Line(origin = {-21.9389, -29.7251}, points = {{0, 8}, {0, 0}}, thickness = 0.75), Line(origin = {-13.242, -28.0635}, points = {{-12, 2}, {-5, 2}}, thickness = 1), Line(origin = {-63.2366, 14.4734}, points = {{0, 8}, {0, 0}}, thickness = 0.75), Line(origin = {-54.5397, 16.135}, points = {{-12, 2}, {-5, 2}}, thickness = 1), Line(origin = {-76.2137, -75.4503}, points = {{0, 8}, {0, 0}}, thickness = 0.75), Line(origin = {-67.5168, -73.7887}, points = {{-12, 2}, {-5, 2}}, thickness = 1), Ellipse(origin = {-7, 23}, lineColor = {138, 226, 52}, fillColor = {4, 51, 2}, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-11, 11}, {11, -11}}, endAngle = 360), Ellipse(origin = {45, -73}, lineColor = {138, 226, 52}, fillColor = {4, 51, 2}, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-11, 11}, {11, -11}}, endAngle = 360), Ellipse(origin = {-67, -27}, lineColor = {138, 226, 52}, fillColor = {4, 51, 2}, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-11, 11}, {11, -11}}, endAngle = 360), Ellipse(origin = {61, 83}, lineColor = {138, 226, 52}, fillColor = {4, 51, 2}, fillPattern = FillPattern.Sphere, lineThickness = 0.75, extent = {{-11, 11}, {11, -11}}, endAngle = 360)}));
end AcidBase;