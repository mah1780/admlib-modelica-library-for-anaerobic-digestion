within ADMLib.AcidBase;

model pH
  ADMLib.Types.Concentration CHplus;
  ADMLib.Types.pH pH;
equation
  pH = -log(CHplus);
end pH;