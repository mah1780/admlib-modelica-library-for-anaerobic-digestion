within ADMLib.AcidBase;

model AcidBaseEquilibriumRate
  extends ADMLib.Interfaces.AcidBaseEquilibriumProcess;
  ADMLib.Types.AcidBaseEquilibriumConstant Ka;
  ADMLib.Types.StoichiometricCoefficient feq;
  ADMLib.Types.Concentration CHplus;
  ADMLib.Types.Concentration C;
equation
  r = Ka / (Ka + CHplus) * der(C);
  feeq = feq;
end AcidBaseEquilibriumRate;
